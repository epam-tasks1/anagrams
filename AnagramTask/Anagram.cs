﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace AnagramTask
{
    public class Anagram
    {
        private readonly string anagramWord;

        public Anagram(string sourceWord)
        {
            if (sourceWord is null)
            {
                throw new ArgumentNullException(nameof(sourceWord));
            }

            if (sourceWord.Length == 0)
            {
                throw new ArgumentException("SourceWord is empty.");
            }

            this.anagramWord = sourceWord.ToLower(CultureInfo.CurrentCulture);
        }

        public string[] FindAnagrams(string[] candidates)
        {
            if (candidates is null)
            {
                throw new ArgumentNullException(nameof(candidates));
            }

            List<string> anagramsWords = new List<string>();

            char[] wordArray = this.anagramWord.ToCharArray();
            Array.Sort(wordArray);

            for (int i = 0; i < candidates.Length; i++)
            {
                char[] candidateArray = candidates[i].ToLower(CultureInfo.CurrentCulture).ToCharArray();
                Array.Sort(candidateArray);
                if (new string(wordArray) == new string(candidateArray) && this.anagramWord != candidates[i].ToLower(CultureInfo.CurrentCulture))
                {
                    anagramsWords.Add(candidates[i]);
                }
            }

            return anagramsWords.ToArray();
        }
    }
}
